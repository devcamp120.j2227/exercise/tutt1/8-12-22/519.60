import { useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css";
import { Input, Button } from "reactstrap";

const Signup = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailSignUp, setEmailSingUp] = useState("");
    const [passwordSignUp, setPasswordSignUp] = useState("");

    const changeFirstNameHandler = (event) => {
        setFirstName(event.target.value);
    };

    const changeLastNameHandler = (event) => {
        setLastName(event.target.value);
    };

    const changeSignUpEmailHandler = (event) => {
        setEmailSingUp(event.target.value);
    };

    const changeSignUpPasswordHandler = (event) => {
        setPasswordSignUp(event.target.value);
    };
    const buttonSignUp = () => {
        if(firstName === "") {
            alert("vui lòng nhập First Name");
            return false;
        }
        if(lastName === "") {
            alert("vui lòng nhập Last Name");
            return false;
        }
        if(emailSignUp === "") {
            alert("vui lòng nhập email");
            return false;
        }
        if(passwordSignUp === "") {
            alert("vui lòng nhập password");
            return false;
        }
        console.log("First Name: ", firstName);
        console.log("Last Name: ", lastName);
        console.log("Email: ", emailSignUp);
        console.log("Password: ", passwordSignUp);
        return true;
    };
    

    return (
        <>
            <form>
                <div className="row text-center mt-5">
                    <h3>Sign Up for Free</h3>
                </div>
                <div className="row mt-5">
                    <Input onChange={changeFirstNameHandler} type="text" placeholder="First Name" style={{"color":"white","width": "50%"}} className="background"/>
                    <Input onChange={changeLastNameHandler} type="text" placeholder="Last Name" style={{"color":"white","width": "50%"}} className="background"/>
                </div>
                <div className="row mt-4">
                    <Input  onChange={changeSignUpEmailHandler} type="email" placeholder="Email address" className="background" style={{"color":"white"}}/>
                </div>
                <div className="row mt-4">
                    <Input onChange={changeSignUpPasswordHandler} type="password" placeholder="Set A Password" className="background" style={{"color":"white"}}/>
                </div>
                <div className="row mt-4">
                    <Button onClick={buttonSignUp} className="greenColor" style={{"borderRadius": "0%"}}>SIGNUP</Button>
                </div>
            </form>
        </>
    )
}

export default Signup