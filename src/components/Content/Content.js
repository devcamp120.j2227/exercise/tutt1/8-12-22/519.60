import { useState } from "react"
import { Button } from "reactstrap"
import Login from "../login/login"
import Signup from "../signup/signup"

const Content = () => {
    const [showForm, setShowForm] = useState(true);
    const loginForm = () => {
        setShowForm(true);
    }
    const signupForm = () => {
        setShowForm(false);
    }

    return (
        <div className="card background form-size p-5">
            <ul className="nav">
                <li className="nav-item">
                    <Button style={{backgroundColor: showForm ? "rgb(36, 51, 60)" : "rgb(26, 177, 136)", color: "white", cursor: 'pointer'}} onClick={signupForm} className="button">Sign Up</Button>
                </li>
                <li className="nav-item">
                    <Button style= {{backgroundColor: showForm ? "rgb(26, 177, 136)" : "rgb(36, 51, 60)", color: "white", cursor: 'pointer'}} onClick={loginForm} className="button">Login</Button>
                </li>
            </ul>
            { showForm ? <Login/> : <Signup/> }
        </div>
    )
}

export default Content