import { useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Input } from "reactstrap";

const Login = () => {
    const [inpEmail, setInpEmail] = useState("");
    const [inpPassword, setInpPassword] = useState("");

    const inpEmailChangeHandler = (event) => {
        setInpEmail(event.target.value);
    }
    const inpPasswordChangeHandler = (event) => {
        setInpPassword(event.target.value);
    }
    const buttonLogInHandler = () => {
        if(inpEmail === "") {
            alert("vui lòng nhập email");
            return false;
        }
        if(inpPassword === "") {
            alert("vui lòng nhập password");
            return false;
        }
        console.log("Email: ", inpEmail);
        console.log("Password: ", inpPassword);
        return true
    };
    

    return (
        <>
            <form>
                <div className="row text-center mt-5">
                    <h3>Welcome Back!</h3>
                </div>
                <div className="row mt-5">
                    <div className="col-sm-12">
                        <Input onChange={inpEmailChangeHandler} type="email" placeholder="Email address" className="background" style={{"color":"white"}}/>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-sm-12">
                        <Input onChange={inpPasswordChangeHandler} type="password" placeholder="Password" className="background" style={{"color":"white"}}/>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-sm-4"></div>
                    <div className="col-sm-3"></div>
                    <div className="col-sm-5">
                        <a href="#" style={{"color": "rgb(26, 177, 136)"}}>Forgot Password?</a>
                    </div>
                </div>
                <div className="row mt-2">
                    <Button onClick={buttonLogInHandler} className="greenColor" style={{"borderRadius": "0%"}}>LOGIN</Button>
                </div>
            </form>
        </>
    )
}

export default Login