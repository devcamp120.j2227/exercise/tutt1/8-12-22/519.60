import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Content from "./components/Content/Content";

function App() {
  return (
    <>
      <Content/>
    </>
  );
}

export default App;